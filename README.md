ArchivesSpace Merger
=================================
This is a customized repository merge SQL script to take a 
source repository from another ArchivesSpace instance and merge it into
another instance, maintaining it's separate repository. This has been tested only 
with ArchivesSpace v2.7.1.

The steps to run the SQL script are within the [archivesspace_db_merge.sql](archivesspace_db_merge.sql) file. 
This document contains instructions for performing the entire merge process. 

Another helpful resource is 
[MSU's customized Docker installation of ArchivesSpace](https://gitlab.msu.edu/msu-libraries/public/archivesspace-docker) which is used for our new merged instance.


Merge Steps
--------------

* Get a dump of both production databases
```
mysqldump source_db > source_db.sql
mysqldump target_db > target_db.sql
```

* Load `source_db` into a new temporary database called `source_db`
```
mysql source_db < source_db.sql
```

* Load the `target_db` data into the database of the merged instance
```
mysql merged_db < target_db.sql
```

* Wipe the solr index on the merged instance
```
curl -s http://[SOLR_HOST]/solr/[MERGED_CORE_NAME]/update --data-binary '<delete><query>*:*</query></delete>' -H 'Content-type:text/xml; charset=utf-8'
rm -rf /archivesspace/data/*
```

* Restart ArchivesSpace and create the new repository through the interface. This will 
be the home of the source instance's repository.

* Stop ArchivesSpace

* Make the updates to the `archivesspace_db_merge.sql` script described in the 
comment block, setting the variables in the Setup section. Most noteably, the repository IDs
for the source and target repositories. You can get this by querying the repository table if necessary.

* Run the archivesspace_db_merge.sql script
```
mysql merged_db < archivesspace_db_merge.sql
```

* Wipe the solr index on the merged instance again
```
curl -s http://[SOLR_HOST]/solr/[MERGED_CORE_NAME]/update --data-binary '<delete><query>*:*</query></delete>' -H 'Content-type:text/xml; charset=utf-8'
rm -rf /archivesspace/data/*
```

* Restart ArchivesSpace and manually edit both repository records and save them via the `/admin` page. You will manually add in 
the information on the source repository's record since the merge script does not import that data. Saving both records will trigger 
them to be reindexed (for some reason including them in the merge script does not allow them to be indexed).

* Verify that the index is increasing either by viewing the `archivesspace.out` log, Solr access log, or Solr core document count.

* Once complete, verify the counts of the search results match the total from each repository via the public search page 
which will show the counts for each repository in the facets.

