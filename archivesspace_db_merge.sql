
#################################################################################################
###  ArchivesSpace Database Merge                                                             ###
###  Tested with: v2.7.1                                                                      ###
###  ---------------------------------------------------------------------------------------  ###
###  Assumptions:                                                                             ###
###  ------------                                                                             ###
###  * The source and target databases live on the same database server.                      ###
###  * You are running this as a user with at least read access to the source database and    ###
###    write access to the target database.                                                   ###
###  * ArchivesSpace is not running on either the source or target instances.                 ###
###  * You have taken a backup of the databases before running this.                          ###
###                                                                                           ###
###  Caveats:                                                                                 ###
###  --------                                                                                 ###
###  * This was only run and tested with ArchivesSpace version 2.7.1.                         ###
###  * This only includes tables populated in our source database at the time of              ###
###    writing this. Below is a list of the tables excluded due to one of the following       ###
###    conditions: the source table was empty, the source and target tables match,            ###
###    the data was not required to migrate (i.e. active_edit and job record tables).         ###
###      active_edit, agent_software, ark_name, assessment, assessment_attribute,             ###
###      assessment_attribute_note, assessment_reviewer_rlshp, assessment_rlshp,              ###
###      classification, classification_creator_rlshp, classification_rlshp,                  ###
###      classification_term, classification_term_creator_rlshp, container_profile,           ###
###      custom_report_template, deleted_records, digital_object_component, enumeration,      ###
###      external_id, import_job_created_record, job_created_record, job_modified_record,     ###
###      location_function, location_profile, location_profile_rlshp, name_software,          ###
###      notification, oai_config, permission, related_accession_rlshp,                       ###
###      related_agents_rlshp, required_fields, rights_statement_act,                         ###
###      rights_statement_pre_088, schema_info, session, surveyed_by_rlshp,                   ###
###      system_event, vocabulary                                                             ###
###                                                                                           ###
###  Development Notes:                                                                       ###
###  ------------------                                                                       ###
###  * This is organized by waves, each representing the set of tables that can be inserted   ###
###    into based on their foreign-key contraints having been migrated already.               ###
###  * Within each wave, the tables are orga=nized alphabetically.                            ###
###  * The max ID of tables is captured and then rounded up to the nearest 1000 to allow      ###
###    for easy mapping of foreign-key identifiers. The exception to this is the              ###
###    repository ID. which is stored in the variable @target_repo_id.                        ###
###  * A break-down of every table with notes is available:                                   ###
###    https://docs.google.com/spreadsheets/d/14hZNHjyPQCLvAiboME25vl_evoBpuJAai8fbJyK8kQA    ###
###                                                                                           ###
###  To Run:                                                                                  ###
###  -------                                                                                  ###
###  * Replace `archivesspace_prod` with your source database name.                           ###
###  * Replace `findingaids` with your target database name.                                  ###
###  * Replace the `source_repo_id` and `target_repo_id` values as appropriate in the         ###
###    Setup section.                                                                         ###
###  * Run the SELECT statement before the note_persistent_id INSERT to verify all CASE       ###
###    conditions are covered.                                                                ###
#################################################################################################


####################################################
###                  Setup                       ###
####################################################

## Set the repository to import
SET @source_repo_id = 2;
SET @target_repo_id = 4;

## Check all note_persistent_id scenarios
## If you see results that are not in: 
## agent_family, agent_person, digital_object, or resource
## you will need to go down to the note_persistent_id SQL 
## to add them to the case statement.
SELECT DISTINCT parent_type FROM archivesspace_prod.note_persistent_id;


####################################################
###               Wave 1 Tables                  ###
####################################################

## agent_corporate_entity
INSERT INTO findingaids_devel.agent_corporate_entity
    (lock_version, json_schema_version, publish, created_by, last_modified_by, create_time,
    system_mtime, user_mtime, agent_sha1, slug, is_slug_auto)
SELECT
    lock_version, json_schema_version, publish, created_by, last_modified_by, create_time,
    system_mtime, user_mtime, agent_sha1, slug, is_slug_auto
FROM archivesspace_prod.agent_corporate_entity
ON DUPLICATE KEY UPDATE agent_corporate_entity.system_mtime = findingaids_devel.agent_corporate_entity.system_mtime;

DROP TEMPORARY TABLE IF EXISTS agent_corporate_entity_mapping;
CREATE TEMPORARY TABLE agent_corporate_entity_mapping
SELECT
	source_ac.id as source_id, target_ac.id as target_id
FROM findingaids_devel.agent_corporate_entity as target_ac
	LEFT JOIN archivesspace_prod.agent_corporate_entity as source_ac
		ON target_ac.agent_sha1 = source_ac.agent_sha1;

## agent_family
SELECT @max_agent_family:=COALESCE(MAX(id),1) FROM findingaids_devel.agent_family;
INSERT INTO findingaids_devel.agent_family
    (id, lock_version, json_schema_version, publish, created_by, last_modified_by, create_time,
    system_mtime, user_mtime, agent_sha1, slug, is_slug_auto)
SELECT
    id + CEILING((@max_agent_family + 1)/1000.0) * 1000, lock_version, json_schema_version, publish, created_by, last_modified_by, create_time,
    system_mtime, user_mtime, agent_sha1, slug, is_slug_auto
FROM archivesspace_prod.agent_family;

## agent_person
SELECT @max_agent_person:=COALESCE(MAX(id),1) FROM findingaids_devel.agent_person;
INSERT INTO findingaids_devel.agent_person
    (id, lock_version, json_schema_version, publish, created_by, last_modified_by, create_time,
    system_mtime, user_mtime, agent_sha1, slug, is_slug_auto)
SELECT
    id + CEILING((@max_agent_person + 1)/1000.0) * 1000, lock_version, json_schema_version, publish, created_by, last_modified_by, create_time,
    system_mtime, user_mtime, agent_sha1, slug, is_slug_auto
FROM archivesspace_prod.agent_person;

## auth_db
INSERT INTO findingaids_devel.auth_db
	(username, create_time, system_mtime, pwhash)
SELECT
	username, create_time, system_mtime, pwhash
FROM archivesspace_prod.auth_db source_a
ON DUPLICATE KEY UPDATE system_mtime=source_a.system_mtime;

## enumeration_value
INSERT INTO findingaids_devel.enumeration_value
    (enumeration_id, value, readonly, position,
    suppressed, lock_version,json_schema_version, created_by,
    last_modified_by, create_time, system_mtime, user_mtime)
SELECT
    mapping.target_enum_id,
    source_ev.value,
    source_ev.readonly, (source_ev.position + target_max_pos.max_pos + 1),
    source_ev.suppressed, source_ev.lock_version, source_ev.json_schema_version, source_ev.created_by,
    source_ev.last_modified_by, source_ev.create_time, source_ev.system_mtime, source_ev.user_mtime
FROM archivesspace_prod.enumeration_value AS source_ev
LEFT JOIN (
    SELECT DISTINCT
    se.id source_enum_id, te.id target_enum_id
    FROM archivesspace_prod.enumeration_value AS sev
    LEFT JOIN archivesspace_prod.enumeration AS se ON (se.id = sev.enumeration_id)
    LEFT JOIN findingaids_devel.enumeration AS te ON (se.name = te.name)
    LEFT JOIN findingaids_devel.enumeration_value AS tev ON (tev.enumeration_id = te.id)
    WHERE tev.value = sev.value) as mapping ON (mapping.source_enum_id = source_ev.enumeration_id)
LEFT JOIN (
    SELECT enumeration_id, MAX(position) AS max_pos
    FROM findingaids_devel.enumeration_value
    GROUP BY enumeration_id) AS target_max_pos ON (target_max_pos.enumeration_id = mapping.target_enum_id)
LEFT JOIN findingaids_devel.enumeration_value AS target_ev ON (target_ev.enumeration_id = mapping.target_enum_id and target_ev.value = source_ev.value)
WHERE target_ev.id IS NULL;

DROP TEMPORARY TABLE IF EXISTS enumeration_mapping;
CREATE TEMPORARY TABLE enumeration_mapping
SELECT
	source_ev.id as source_id, target_ev.id as target_id
FROM archivesspace_prod.enumeration_value as source_ev
	LEFT JOIN archivesspace_prod.enumeration as source_e
		ON source_e.id = source_ev.enumeration_id
	LEFT JOIN findingaids_devel.enumeration target_e
		ON target_e.name = source_e.name
	LEFT JOIN findingaids_devel.enumeration_value target_ev
		ON target_ev.value = source_ev.value
		AND target_ev.enumeration_id = target_e.id;

## rde_template
INSERT INTO findingaids_devel.rde_template
	(record_type, `name`, `order`, `visible`, defaults, created_by, last_modified_by,
    create_time, system_mtime, user_mtime)
SELECT
	record_type, `name`, `order`, `visible`, defaults, created_by, last_modified_by,
    create_time, system_mtime, user_mtime
FROM archivesspace_prod.rde_template;

####################################################
###               Wave 2 Tables                  ###
####################################################

## agent_contact
SELECT @max_agent_contact:=COALESCE(MAX(id),1) FROM findingaids_devel.agent_contact;
INSERT INTO findingaids_devel.agent_contact
	(id, lock_version, json_schema_version, agent_person_id, agent_family_id,
    agent_corporate_entity_id, agent_software_id, `name`, salutation_id,
    address_1, address_2, address_3, city, region, country, post_code, email,
    email_signature, note, created_by, last_modified_by,
    create_time, system_mtime, user_mtime)
SELECT
	source_a.id + CEILING((@max_agent_contact + 1)/1000.0) * 1000, source_a.lock_version, source_a.json_schema_version, 
    source_a.agent_person_id + CEILING((@max_agent_person + 1)/1000.0) * 1000, 
    source_a.agent_family_id + CEILING((@max_agent_family + 1)/1000.0) * 1000, 
    ac_map.target_id, 
    target_as.id, source_a.`name`, target_ev_s.id,
    source_a.address_1, source_a.address_2, source_a.address_3, source_a.city, source_a.region, 
    source_a.country, source_a.post_code, source_a.email,
    source_a.email_signature, note, source_a.created_by, source_a.last_modified_by,
    source_a.create_time, source_a.system_mtime, source_a.user_mtime
FROM archivesspace_prod.agent_contact source_a
# Map agent_corporate_entity
LEFT JOIN agent_corporate_entity_mapping ac_map
	ON ac_map.source_id = source_a.agent_corporate_entity_id
# Map salution ID
LEFT JOIN archivesspace_prod.enumeration_value source_ev_s
    ON source_ev_s.id = source_a.salutation_id
LEFT JOIN archivesspace_prod.enumeration source_e_s
    ON source_e_s.id = source_ev_s.enumeration_id
# Map agent_software ID
LEFT JOIN archivesspace_prod.agent_software source_as
	ON source_as.id = source_a.agent_software_id
# Map salutation to target ID
LEFT JOIN findingaids_devel.enumeration target_e_s
	ON target_e_s.name = source_e_s.name
LEFT JOIN findingaids_devel.enumeration_value target_ev_s
	ON target_ev_s.value = source_ev_s.value
	AND target_ev_s.enumeration_id = target_e_s.id
# Map agent_software to target ID
LEFT JOIN findingaids_devel.agent_software target_as
	ON target_as.system_role = source_as.system_role;


## container_profile
SELECT @max_container_profile:=COALESCE(MAX(id),1) FROM findingaids_devel.container_profile;
INSERT INTO findingaids_devel.container_profile
	(id, lock_version, `name`, url, extent_dimension, dimension_units_id, height,
    width, depth, created_by, last_modified_by, create_time, system_mtime, 
    user_mtime, stacking_limit)
SELECT
	source_c.id + CEILING((@max_container_profile + 1)/1000.0) * 1000, 
    source_c.lock_version, source_c.`name`, source_c.url, source_c.extent_dimension, 
    em_du.target_id, source_c.height,
    source_c.width, source_c.depth, source_c.created_by, source_c.last_modified_by, 
    source_c.create_time, source_c.system_mtime, 
    source_c.user_mtime, source_c.stacking_limit
FROM archivesspace_prod.container_profile source_c
	LEFT JOIN enumeration_mapping em_du
		ON em_du.source_id = source_c.dimension_units_id;SELECT
	source_c.id + CEILING((@max_container_profile + 1)/1000.0) * 1000, 
    source_c.lock_version, source_c.`name`, source_c.url, source_c.extent_dimension, 
    em_du.target_id, source_c.height,
    source_c.width, source_c.depth, source_c.created_by, source_c.last_modified_by, 
    source_c.create_time, source_c.system_mtime, 
    source_c.user_mtime, source_c.stacking_limit
FROM archivesspace_prod.container_profile source_c
	LEFT JOIN enumeration_mapping em_du
		ON em_du.source_id = source_c.dimension_units_id;


## location
SELECT @max_location:=COALESCE(MAX(id),1) FROM findingaids_devel.location;
INSERT INTO findingaids_devel.location
	(id, lock_version,
	json_schema_version, building, title, floor,
	room, area, barcode, classification,
	coordinate_1_label, coordinate_1_indicator,
	coordinate_2_label, coordinate_2_indicator,
	coordinate_3_label, coordinate_3_indicator,
	temporary_id, created_by, last_modified_by,
	create_time, system_mtime, user_mtime)
SELECT 
	source_l.id + CEILING((@max_location + 1)/1000.0) * 1000, source_l.lock_version,
	source_l.json_schema_version, source_l.building, source_l.title, source_l.floor,
	source_l.room, source_l.area, source_l.barcode, source_l.classification,
	source_l.coordinate_1_label, source_l.coordinate_1_indicator,
	source_l.coordinate_2_label, source_l.coordinate_2_indicator,
	source_l.coordinate_3_label, source_l.coordinate_3_indicator,
	target_ev_ti.id, source_l.created_by, source_l.last_modified_by,
	source_l.create_time, source_l.system_mtime, source_l.user_mtime
FROM archivesspace_prod.location source_l
# Map temorary ID
LEFT JOIN archivesspace_prod.enumeration_value source_ev_ti
	ON source_ev_ti.id = source_l.temporary_id
LEFT JOIN archivesspace_prod.enumeration source_e_ti
	ON source_e_ti.id = source_ev_ti.enumeration_id
# Map to temporary ID
LEFT JOIN findingaids_devel.enumeration target_e_ti
	ON target_e_ti.name = source_e_ti.name
LEFT JOIN findingaids_devel.enumeration_value target_ev_ti
	ON target_ev_ti.value = source_ev_ti.value
	AND target_ev_ti.enumeration_id = target_e_ti.id;

## name_corporate_entity
INSERT INTO name_corporate_entity
	(lock_version, json_schema_version, agent_corporate_entity_id,
    primary_name, subordinate_name_1, subordinate_name_2, `number`,
    dates, qualifier, source_id, rules_id, sort_name, sort_name_auto_generate,
    created_by, last_modified_by, create_time, system_mtime, user_mtime, authorized, is_display_name)
SELECT
	source_ce.lock_version, source_ce.json_schema_version, 
    ac_map.target_id, source_ce.primary_name, 
    source_ce.subordinate_name_1, source_ce.subordinate_name_2, `number`,
    source_ce.dates, source_ce.qualifier, target_ev_s.id, target_ev_r.id, source_ce.sort_name, source_ce.sort_name_auto_generate,
    source_ce.created_by, source_ce.last_modified_by, source_ce.create_time, source_ce.system_mtime, source_ce.user_mtime, source_ce.authorized, source_ce.is_display_name
FROM archivesspace_prod.name_corporate_entity source_ce
# Map agent_corporate_entity
LEFT JOIN agent_corporate_entity_mapping ac_map
	ON ac_map.source_id = source_ce.agent_corporate_entity_id
# Map source_id
LEFT JOIN archivesspace_prod.enumeration_value source_ev_s
	ON source_ev_s.id = source_ce.source_id
LEFT JOIN archivesspace_prod.enumeration source_e_s
	ON source_e_s.id = source_ev_s.enumeration_id
# Map rules_id
LEFT JOIN archivesspace_prod.enumeration_value source_ev_r
	ON source_ev_r.id = source_ce.rules_id
LEFT JOIN archivesspace_prod.enumeration source_e_r
	ON source_e_r.id = source_ev_r.enumeration_id
# Map source_id to target
LEFT JOIN findingaids_devel.enumeration target_e_s
	ON target_e_s.name = source_e_s.name
LEFT JOIN findingaids_devel.enumeration_value target_ev_s
	ON target_ev_s.value = source_ev_s.value
	AND target_ev_s.enumeration_id = target_e_s.id
# Map rules_id to target
LEFT JOIN findingaids_devel.enumeration target_e_r
	ON target_e_r.name = source_e_r.name
LEFT JOIN findingaids_devel.enumeration_value target_ev_r
	ON target_ev_r.value = source_ev_r.value
	AND target_ev_r.enumeration_id = target_e_r.id
ON DUPLICATE KEY UPDATE system_mtime = source_ce.system_mtime;

DROP TEMPORARY TABLE IF EXISTS name_corporate_entity_mapping;
CREATE TEMPORARY TABLE name_corporate_entity_mapping
SELECT
	source_nc.id as source_id, target_nc.id as target_id
FROM findingaids_devel.name_corporate_entity as target_nc
	LEFT JOIN agent_corporate_entity_mapping ac_map
		ON ac_map.target_id = target_nc.id
	LEFT JOIN findingaids_devel.name_corporate_entity as source_nc
		ON target_nc.authorized = source_nc.authorized
        AND target_nc.is_display_name = source_nc.is_display_name
        AND source_nc.agent_corporate_entity_id = ac_map.source_id;

## name_family
SELECT @max_name_family:=COALESCE(MAX(id),1) FROM findingaids_devel.name_family;
INSERT INTO findingaids_devel.name_family
	(id, lock_version, json_schema_version, agent_family_id,
    family_name, prefix, dates, qualifier, source_id, rules_id, sort_name,
    sort_name_auto_generate, created_by, last_modified_by,
    create_time, system_mtime, user_mtime, authorized, is_display_name)
SELECT
	source_n.id + CEILING((@max_name_family + 1)/1000.0) * 1000, source_n.lock_version, source_n.json_schema_version, 
    source_n.agent_family_id + CEILING((@max_agent_family + 1)/1000.0) * 1000, source_n.family_name, source_n.prefix, 
    source_n.dates, source_n.qualifier, 
    target_ev_s.id, target_ev_r.id, source_n.sort_name,
    source_n.sort_name_auto_generate, source_n.created_by, source_n.last_modified_by,
    source_n.create_time, source_n.system_mtime, source_n.user_mtime, source_n.authorized, source_n.is_display_name
FROM archivesspace_prod.name_family source_n
# Map source_id
LEFT JOIN archivesspace_prod.enumeration_value source_ev_s
	ON source_ev_s.id = source_n.source_id
LEFT JOIN archivesspace_prod.enumeration source_e_s
	ON source_e_s.id = source_ev_s.enumeration_id
# Map rules_id
LEFT JOIN archivesspace_prod.enumeration_value source_ev_r
	ON source_ev_r.id = source_n.rules_id
LEFT JOIN archivesspace_prod.enumeration source_e_r
	ON source_e_r.id = source_ev_r.enumeration_id
# Map source_id to target
LEFT JOIN findingaids_devel.enumeration target_e_s
	ON target_e_s.name = source_e_s.name
LEFT JOIN findingaids_devel.enumeration_value target_ev_s
	ON target_ev_s.value = source_ev_s.value
	AND target_ev_s.enumeration_id = target_e_s.id
# Map rules_id to target
LEFT JOIN findingaids_devel.enumeration target_e_r
	ON target_e_r.name = source_e_r.name
LEFT JOIN findingaids_devel.enumeration_value target_ev_r
	ON target_ev_r.value = source_ev_r.value
	AND target_ev_r.enumeration_id = target_e_r.id;

## name_person
SELECT @max_name_person:=COALESCE(MAX(id),1) FROM findingaids_devel.name_person;
INSERT INTO findingaids_devel.name_person
	(id, lock_version, json_schema_version, agent_person_id,
    primary_name, name_order_id, title, prefix, rest_of_name,
    suffix, fuller_form, `number`, dates, qualifier,
    source_id, rules_id, sort_name, sort_name_auto_generate,
    created_by, last_modified_by, create_time, system_mtime,
    user_mtime, authorized, is_display_name)
SELECT
	source_n.id + CEILING((@max_name_person + 1)/1000.0) * 1000, source_n.lock_version, source_n.json_schema_version, 
    source_n.agent_person_id + CEILING((@max_agent_person + 1)/1000.0) * 1000,
    source_n.primary_name, target_ev_n.id, source_n.title, source_n.prefix, source_n.rest_of_name,
    source_n.suffix, source_n.fuller_form, `number`, source_n.dates, source_n.qualifier,
    target_ev_s.id, target_ev_r.id, source_n.sort_name, source_n.sort_name_auto_generate,
    source_n.created_by, source_n.last_modified_by, source_n.create_time, source_n.system_mtime,
    source_n.user_mtime, source_n.authorized, source_n.is_display_name
FROM archivesspace_prod.name_person source_n
# Map source_id
LEFT JOIN archivesspace_prod.enumeration_value source_ev_s
	ON source_ev_s.id = source_n.source_id
LEFT JOIN archivesspace_prod.enumeration source_e_s
	ON source_e_s.id = source_ev_s.enumeration_id
# Map rules_id
LEFT JOIN archivesspace_prod.enumeration_value source_ev_r
	ON source_ev_r.id = source_n.rules_id
LEFT JOIN archivesspace_prod.enumeration source_e_r
	ON source_e_r.id = source_ev_r.enumeration_id
# Map name order id
LEFT JOIN archivesspace_prod.enumeration_value source_ev_n
	ON source_ev_n.id = source_n.name_order_id
LEFT JOIN archivesspace_prod.enumeration source_e_n
	ON source_e_n.id = source_ev_n.enumeration_id
# Map source_id to target
LEFT JOIN findingaids_devel.enumeration target_e_s
	ON target_e_s.name = source_e_s.name
LEFT JOIN findingaids_devel.enumeration_value target_ev_s
	ON target_ev_s.value = source_ev_s.value
	AND target_ev_s.enumeration_id = target_e_s.id
# Map rules_id to target
LEFT JOIN findingaids_devel.enumeration target_e_r
	ON target_e_r.name = source_e_r.name
LEFT JOIN findingaids_devel.enumeration_value target_ev_r
	ON target_ev_r.value = source_ev_r.value
	AND target_ev_r.enumeration_id = target_e_r.id
# Map name order ID to target
LEFT JOIN findingaids_devel.enumeration target_e_n
	ON target_e_n.name = source_e_n.name
LEFT JOIN findingaids_devel.enumeration_value target_ev_n
	ON target_ev_n.value = source_ev_n.value
	AND target_ev_n.enumeration_id = target_e_n.id;
    
## repository


## subject
INSERT INTO findingaids_devel.subject
	(lock_version, json_schema_version,
	vocab_id, title, terms_sha1, authority_id, 
	scope_note, source_id, created_by, 
	last_modified_by, create_time, system_mtime, user_mtime,
	slug, is_slug_auto)
SELECT 
	source_s.lock_version, source_s.json_schema_version, 
	target_v.id, source_s.title, source_s.terms_sha1,
	source_s.authority_id, source_s.scope_note, 
	em.target_id, source_s.created_by, 
	source_s.last_modified_by, source_s.create_time, 
	source_s.system_mtime, source_s.user_mtime,
	source_s.slug, source_s.is_slug_auto
FROM archivesspace_prod.subject source_s
	LEFT JOIN enumeration_mapping em
		ON em.source_id = source_s.source_id
	LEFT JOIN archivesspace_prod.vocabulary source_v
		ON source_s.vocab_id = source_v.id
	LEFT JOIN findingaids_devel.vocabulary target_v
		ON target_v.name = source_v.name
ON DUPLICATE KEY UPDATE authority_id = IFNULL(findingaids_devel.subject.authority_id, source_s.authority_id);

DROP TEMPORARY TABLE IF EXISTS subject_mapping;
CREATE TEMPORARY TABLE subject_mapping
SELECT 
	source_s.id as source_id, target_s.id as target_id
FROM archivesspace_prod.`subject` source_s
	LEFT JOIN enumeration_mapping em
		ON em.source_id = source_s.source_id
	LEFT JOIN archivesspace_prod.vocabulary source_v
		ON source_s.vocab_id = source_v.id
	LEFT JOIN findingaids_devel.vocabulary target_v
		ON target_v.name = source_v.name
	LEFT JOIN findingaids_devel.`subject` target_s
		ON target_s.vocab_id = target_v.id
        AND target_s.source_id = em.target_id
        AND ((target_s.terms_sha1 = source_s.terms_sha1 AND target_s.authority_id = source_s.authority_id)
			  OR target_s.terms_sha1 = source_s.terms_sha1);

## term
INSERT INTO findingaids_devel.term
	(lock_version, json_schema_version, vocab_id,
    term, term_type_id, created_by, last_modified_by, create_time,
    system_mtime, user_mtime)
SELECT
	source_t.lock_version, source_t.json_schema_version, target_v.id,
    source_t.term, target_ev.id, source_t.created_by, source_t.last_modified_by,
    source_t.create_time, source_t.system_mtime, source_t.user_mtime
FROM archivesspace_prod.term source_t
	LEFT JOIN archivesspace_prod.enumeration_value source_ev
		ON source_ev.id = source_t.term_type_id
	LEFT JOIN archivesspace_prod.enumeration source_e
		ON source_e.id = source_ev.enumeration_id
	LEFT JOIN archivesspace_prod.vocabulary source_v
		ON source_t.vocab_id = source_v.id
	LEFT JOIN findingaids_devel.enumeration target_e
		ON target_e.name = source_e.name
	LEFT JOIN findingaids_devel.enumeration_value target_ev
		ON target_ev.value = source_ev.value
		AND target_ev.enumeration_id = target_e.id
	LEFT JOIN findingaids_devel.vocabulary target_v
		ON target_v.name = source_v.name
ON DUPLICATE KEY UPDATE system_mtime = source_t.system_mtime;

DROP TEMPORARY TABLE IF EXISTS term_mapping;
CREATE TEMPORARY TABLE term_mapping
SELECT 
	source_t.id as source_id, target_t.id as target_id
FROM archivesspace_prod.term source_t
	LEFT JOIN archivesspace_prod.enumeration_value source_ev
		ON source_ev.id = source_t.term_type_id
	LEFT JOIN archivesspace_prod.enumeration source_e
		ON source_e.id = source_ev.enumeration_id
	LEFT JOIN archivesspace_prod.vocabulary source_v
		ON source_t.vocab_id = source_v.id
	LEFT JOIN findingaids_devel.enumeration target_e
		ON target_e.name = source_e.name
	LEFT JOIN findingaids_devel.enumeration_value target_ev
		ON target_ev.value = source_ev.value
		AND target_ev.enumeration_id = target_e.id
	LEFT JOIN findingaids_devel.vocabulary target_v
		ON target_v.name = source_v.name
	LEFT JOIN findingaids_devel.term target_t
		ON target_t.vocab_id = target_v.id
        AND target_t.term = source_t.term
        AND target_t.term_type_id = target_ev.id;
        
## user
INSERT INTO findingaids_devel.user
	(lock_version, json_schema_version, username, name,
    source, agent_record_id, agent_record_type, is_system_user, is_hidden_user, email,
    first_name, last_name, telephone, title, department, additional_contact, created_by,
    last_modified_by, create_time, system_mtime, user_mtime)
SELECT
	source_u.lock_version, source_u.json_schema_version, source_u.username, source_u.name,
    source_u.source, source_u.agent_record_id + CEILING((@max_agent_person + 1)/1000.0) * 1000, source_u.agent_record_type, 
    source_u.is_system_user, source_u.is_hidden_user, source_u.email,
    source_u.first_name, source_u.last_name, source_u.telephone, source_u.title, source_u.department, source_u.additional_contact, 
    source_u.created_by, source_u.last_modified_by, source_u.create_time, source_u.system_mtime, source_u.user_mtime
FROM archivesspace_prod.user source_u
LEFT JOIN findingaids_devel.user target_u ON target_u.username = source_u.username
WHERE target_u.id IS NULL;

####################################################
###               Wave 3 Tables                  ###
####################################################

## accession
SELECT @max_accession:=COALESCE(MAX(id),1) FROM findingaids_devel.accession;
INSERT INTO findingaids_devel.accession
	(id, lock_version, json_schema_version, repo_id,
	suppressed, identifier, title, display_string, publish,
	content_description, condition_description, disposition, inventory,
	provenance, general_note,
	resource_type_id, acquisition_type_id,
	accession_date, restrictions_apply, retention_rule,
	access_restrictions, access_restrictions_note, use_restrictions,
	use_restrictions_note, created_by,
	last_modified_by, create_time, system_mtime, user_mtime, slug,
	is_slug_auto)
SELECT
	source_a.id + CEILING((@max_accession + 1)/1000.0) * 1000,
	source_a.lock_version, source_a.json_schema_version,
	@target_repo_id,
	source_a.suppressed, source_a.identifier, source_a.title, source_a.display_string, source_a.publish,
	source_a.content_description, source_a.condition_description, source_a.disposition, source_a.inventory,
	source_a.provenance, source_a.general_note,
	target_e_rt.id, target_e_at.id,
	source_a.accession_date, source_a.restrictions_apply, source_a.retention_rule,
	source_a.access_restrictions, source_a.access_restrictions_note, source_a.use_restrictions,
	source_a.use_restrictions_note, source_a.created_by,
	source_a.last_modified_by, source_a.create_time, source_a.system_mtime, source_a.user_mtime, source_a.slug,
	source_a.is_slug_auto
FROM
archivesspace_prod.accession source_a
# Get the source enumeration_value records
LEFT JOIN archivesspace_prod.enumeration_value source_e_rt
    ON source_e_rt.id = source_a.resource_type_id
LEFT JOIN archivesspace_prod.enumeration_value source_e_at
    ON source_e_at.id = source_a.acquisition_type_id
# Get the source enumeration records for the enumeration_value records
LEFT JOIN archivesspace_prod.enumeration source_el_at
    ON source_el_at.id = source_e_at.enumeration_id
LEFT JOIN archivesspace_prod.enumeration source_el_rt
    ON source_el_rt.id = source_e_rt.enumeration_id
# Get the target enumeration record that map to the source enumeration record
LEFT JOIN findingaids_devel.enumeration target_el_rt
    ON target_el_rt.name = source_el_rt.name
LEFT JOIN findingaids_devel.enumeration target_el_at
    ON target_el_at.name = source_el_at.name
# Get the target enumeration_value record that maps to the source enumeration record
LEFT JOIN findingaids_devel.enumeration_value target_e_rt
    ON target_e_rt.value = source_e_rt.value
    AND target_e_rt.enumeration_id = target_el_rt.id
LEFT JOIN findingaids_devel.enumeration_value target_e_at
    ON target_e_at.value = source_e_at.value
    AND target_e_at.enumeration_id = target_el_at.id;


## assessment_attribute_definition
SELECT @max_assessment_attribute_definition:=COALESCE(MAX(id),1) FROM findingaids_devel.assessment_attribute_definition;
INSERT INTO findingaids_devel.assessment_attribute_definition
	(id, repo_id, label, `type`, position, readonly)
SELECT
	source_a.id + CEILING((@max_assessment_attribute_definition + 1)/1000.0) * 1000, 
    @target_repo_id, source_a.label, source_a.`type`, source_a.position, source_a.readonly
FROM archivesspace_prod.assessment_attribute_definition source_a
WHERE source_a.repo_id = @source_repo_id;

## default_values
INSERT INTO findingaids_devel.default_values
	(lock_version, id, `blob`, repo_id, record_type, created_by, last_modified_by,
    create_time, system_mtime, user_mtime)
SELECT
	source_d.lock_version, REPLACE(id, @source_repo_id, @target_repo_id),
    source_d.`blob`, @target_repo_id, source_d.record_type, source_d.created_by, source_d.last_modified_by,
    source_d.create_time, source_d.system_mtime, source_d.user_mtime
FROM archivesspace_prod.default_values source_d
WHERE source_d.repo_id = @source_repo_id;


## digital_object
SELECT @max_digital_object:=COALESCE(MAX(id),1) FROM findingaids_devel.digital_object;
INSERT INTO findingaids_devel.digital_object
    (id,
    lock_version, json_schema_version, repo_id,
    digital_object_id, title,
    level_id, digital_object_type_id,
    publish, restrictions, system_generated,
    created_by,
    last_modified_by, create_time,
    system_mtime, user_mtime, suppressed,
    slug, is_slug_auto)
SELECT
    source_d.id + CEILING((@max_digital_object + 1)/1000.0) * 1000,
    source_d.lock_version, source_d.json_schema_version, @target_repo_id,
    source_d.digital_object_id, source_d.title,
    target_ev_level.id as level_id, target_ev_dot.id as digital_object_type_id,
    source_d.publish, source_d.restrictions, source_d.system_generated,
    source_d.created_by,
    source_d.last_modified_by, source_d.create_time,
    source_d.system_mtime, source_d.user_mtime, source_d.suppressed,
    source_d.slug, source_d.is_slug_auto
FROM archivesspace_prod.digital_object source_d
    # Map Level
    LEFT JOIN archivesspace_prod.enumeration_value source_ev_level
        ON source_ev_level.id = source_d.level_id
    LEFT JOIN archivesspace_prod.enumeration source_e_level
        ON source_e_level.id = source_ev_level.enumeration_id
    # Map digital_object_type_id
    LEFT JOIN archivesspace_prod.enumeration_value source_ev_dot
        ON source_ev_dot.id = source_d.digital_object_type_id
    LEFT JOIN archivesspace_prod.enumeration source_e_dot
        ON source_e_dot.id = source_ev_dot.enumeration_id
    # Map to target Level
    LEFT JOIN findingaids_devel.enumeration target_e_level
        ON target_e_level.name = source_e_level.name
    LEFT JOIN findingaids_devel.enumeration_value target_ev_level
        ON target_ev_level.value = source_ev_level.value
        AND target_ev_level.enumeration_id = target_e_level.id
    # Map to target digital_object_type_id
    LEFT JOIN findingaids_devel.enumeration target_e_dot
        ON target_e_dot.name = source_e_dot.name
    LEFT JOIN findingaids_devel.enumeration_value target_ev_dot
        ON target_ev_dot.value = source_ev_dot.value
        AND target_ev_dot.enumeration_id = target_e_dot.id;

## event
SELECT @max_event:=COALESCE(MAX(id),1) FROM findingaids_devel.`event`;
INSERT INTO findingaids_devel.`event`
	(id, lock_version, json_schema_version, suppressed, repo_id,
	event_type_id, outcome_id, outcome_note, `timestamp`, 
	created_by, last_modified_by, create_time,
	system_mtime, user_mtime, refid)
SELECT 
	source_et.id + CEILING((@max_event + 1)/1000.0) * 1000,
    source_et.lock_version, source_et.json_schema_version, 
	source_et.suppressed, @target_repo_id,
	target_ev_et.id, target_ev_o.id, source_et.outcome_note, 
	source_et.`timestamp`, 
	source_et.created_by, source_et.last_modified_by, source_et.create_time,
	source_et.system_mtime, source_et.user_mtime, source_et.refid
FROM archivesspace_prod.`event` source_et
# Map event type id
LEFT JOIN archivesspace_prod.enumeration_value source_ev_et
	ON source_ev_et.id = source_et.event_type_id
LEFT JOIN archivesspace_prod.enumeration source_e_et
	ON source_e_et.id = source_ev_et.enumeration_id
# Map outcome id
LEFT JOIN archivesspace_prod.enumeration_value source_ev_o
	ON source_ev_o.id = source_et.outcome_id
LEFT JOIN archivesspace_prod.enumeration source_e_o
	ON source_e_o.id = source_ev_o.enumeration_id
# Map to event type id
LEFT JOIN findingaids_devel.enumeration target_e_et
	ON target_e_et.name = source_e_et.name
LEFT JOIN findingaids_devel.enumeration_value target_ev_et
	ON target_ev_et.value = source_ev_et.value
	AND target_ev_et.enumeration_id = target_e_et.id
# Map to outcome id
LEFT JOIN findingaids_devel.enumeration target_e_o
	ON target_e_o.name = source_e_o.name
LEFT JOIN findingaids_devel.enumeration_value target_ev_o
	ON target_ev_o.value = source_ev_o.value
	AND target_ev_o.enumeration_id = target_e_o.id;

## group
INSERT INTO findingaids_devel.`group`
	(lock_version, json_schema_version, repo_id,
    group_code, group_code_norm, `description`,
    created_by, last_modified_by, create_time, system_mtime,
    user_mtime)
SELECT
	source_g.lock_version, source_g.json_schema_version, @target_repo_id,
    source_g.group_code, source_g.group_code_norm, source_g.`description`,
    source_g.created_by, source_g.last_modified_by, source_g.create_time, source_g.system_mtime,
    source_g.user_mtime
FROM archivesspace_prod.`group` source_g
WHERE source_g.repo_id = @source_repo_id
ON DUPLICATE KEY UPDATE system_mtime = source_g.system_mtime;


DROP TEMPORARY TABLE IF EXISTS group_mapping;
CREATE TEMPORARY TABLE group_mapping
SELECT
	source_g.id as source_id, target_g.id as target_id
FROM
	archivesspace_prod.`group` as source_g,
	findingaids_devel.`group` as target_g
WHERE
	source_g.repo_id IN (1,@source_repo_id)
	AND target_g.repo_id IN (1, @target_repo_id)
	AND (
		(source_g.repo_id = 1 and target_g.repo_id = 1)
		OR
		(source_g.repo_id = @source_repo_id AND target_g.repo_id = @target_repo_id))
	AND source_g.group_code_norm = target_g.group_code_norm;


## import_job


## job



## name_authority_id
INSERT INTO findingaids_devel.name_authority_id
	(lock_version, name_person_id, name_family_id, name_software_id, name_corporate_entity_id,
    authority_id, created_by, last_modified_by, create_time, system_mtime, user_mtime)
SELECT
	source_n.lock_version, source_n.name_person_id + CEILING((@max_name_person + 1)/1000.0) * 1000, 
    source_n.name_family_id + CEILING((@max_name_family + 1)/1000.0) * 1000, target_s.id, 
    nc_map.target_id,
    source_n.authority_id, source_n.created_by, source_n.last_modified_by, source_n.create_time, source_n.system_mtime, source_n.user_mtime
FROM archivesspace_prod.name_authority_id source_n
	LEFT JOIN archivesspace_prod.name_software source_s
		ON source_s.id = source_n.name_software_id
	LEFT JOIN findingaids_devel.name_software target_s
		ON target_s.software_name = source_s.software_name
	LEFT JOIN name_corporate_entity_mapping nc_map
		ON nc_map.source_id = source_n.name_corporate_entity_id
ON DUPLICATE KEY UPDATE system_mtime = source_n.system_mtime;


## owner_repo_rlhsp
SELECT @max_owner_repo_rlshp:=COALESCE(MAX(id),1) FROM findingaids_devel.owner_repo_rlshp;
INSERT INTO findingaids_devel.owner_repo_rlshp
	(id, location_id, repository_id, aspace_relationship_position,
    suppressed, created_by, last_modified_by, system_mtime, user_mtime)
SELECT
	source_o.id + CEILING((@max_owner_repo_rlshp + 1)/1000.0) * 1000,
    source_o.location_id + CEILING((@max_location + 1)/1000.0) * 1000,
    @target_repo_id, source_o.aspace_relationship_position,
    suppressed, created_by, last_modified_by, system_mtime, user_mtime
FROM archivesspace_prod.owner_repo_rlshp source_o
WHERE source_o.repository_id = @source_repo_id;


## preference
SELECT @max_preference:=COALESCE(MAX(id),1) FROM findingaids_devel.preference;
INSERT INTO findingaids_devel.preference
	(id, lock_version, json_schema_version, repo_id,
    user_id, user_uniq, defaults, created_by, last_modified_by,
    create_time, system_mtime, user_mtime)
SELECT
	source_p.id + CEILING((@max_preference + 1)/1000.0) * 1000, source_p.lock_version, 
    source_p.json_schema_version, @target_repo_id,
    target_u.id, source_p.user_uniq, source_p.defaults, source_p.created_by, source_p.last_modified_by,
    source_p.create_time, source_p.system_mtime, source_p.user_mtime
FROM archivesspace_prod.preference source_p
	LEFT JOIN archivesspace_prod.`user` source_u
		ON source_u.id = source_p.user_id
	LEFT JOIN findingaids_devel.`user` target_u
		ON target_u.username = source_u.username
WHERE source_p.repo_id = @source_repo_id;

## resource
SELECT @max_resource:=COALESCE(MAX(id),1) FROM findingaids_devel.resource;
SET SQL_BIG_SELECTS = 1;
INSERT INTO findingaids_devel.resource
	(id, lock_version, json_schema_version, repo_id,
	accession_id, title, identifier, level_id, other_level,
	resource_type_id, publish, restrictions, 
	repository_processing_note, ead_id, ead_location,
	finding_aid_title, finding_aid_filing_title, finding_aid_date,
	finding_aid_author, finding_aid_description_rules_id, 
	finding_aid_language_note, finding_aid_sponsor,
	finding_aid_edition_statement, finding_aid_series_statement,
	finding_aid_status_id, finding_aid_note,
	system_generated, created_by, last_modified_by,
	create_time, system_mtime, user_mtime, suppressed, 
	finding_aid_subtitle,finding_aid_sponsor_sha1,
	slug, is_slug_auto, finding_aid_language_id, finding_aid_script_id,
	external_ark_url)
SELECT 
	source_r.id + CEILING((@max_resource + 1)/1000.0) * 1000,
	source_r.lock_version, source_r.json_schema_version, @target_repo_id,
	target_a.id, source_r.title, source_r.identifier, 
	em_level.target_id, source_r.other_level,
	em_rt.target_id, source_r.publish, source_r.restrictions, 
	source_r.repository_processing_note, source_r.ead_id, source_r.ead_location,
	source_r.finding_aid_title, source_r.finding_aid_filing_title, source_r.finding_aid_date,
	source_r.finding_aid_author, em_rid.target_id, 
	source_r.finding_aid_language_note, source_r.finding_aid_sponsor,
	source_r.finding_aid_edition_statement, source_r.finding_aid_series_statement,
	em_fasid.target_id, source_r.finding_aid_note,
	source_r.system_generated, source_r.created_by, source_r.last_modified_by,
	source_r.create_time, source_r.system_mtime, source_r.user_mtime, source_r.suppressed, 
	source_r.finding_aid_subtitle, source_r.finding_aid_sponsor_sha1,
	source_r.slug, source_r.is_slug_auto, em_lang.target_id, 
	em_fa.target_id, source_r.external_ark_url
FROM archivesspace_prod.resource source_r
# Map finding aid language
LEFT JOIN enumeration_mapping em_lang
	ON em_lang.source_id = source_r.finding_aid_language_id
# Map finding aid script
LEFT JOIN enumeration_mapping em_fa
	ON em_fa.source_id = source_r.finding_aid_script_id
# Map Level
LEFT JOIN enumeration_mapping em_level
	ON em_level.source_id = source_r.level_id
# Map Resource Type
LEFT JOIN enumeration_mapping em_rt
	ON em_rt.source_id = source_r.resource_type_id
# Map FA description rules id
LEFT JOIN enumeration_mapping em_rid
	ON em_rid.source_id = source_r.finding_aid_description_rules_id
# Map FA status ID
LEFT JOIN enumeration_mapping em_fasid
	ON em_fasid.source_id = source_r.finding_aid_status_id
# Map Accession ID
LEFT JOIN archivesspace_prod.accession source_a
	ON source_a.id = source_r.accession_id
# Map to taget Accession
LEFT JOIN findingaids_devel.accession target_a
	ON target_a.repo_id = @target_repo_id
    AND target_a.identifier = source_a.identifier;
SET SQL_BIG_SELECTS = 0;


## sequence
INSERT INTO findingaids_devel.sequence
	(sequence_name, value)
SELECT
	REPLACE(source_s.sequence_name,CONCAT('/repositories/', @source_repo_id),CONCAT('repositories/',@target_repo_id)),
    source_s.value
FROM archivesspace_prod.sequence source_s
WHERE source_s.sequence_name LIKE CONCAT('/repositories/', @source_repo_id, '%');

## subject_term
SET SQL_BIG_SELECTS = 1;
SELECT @max_subject_term:=COALESCE(MAX(id),1) FROM findingaids_devel.subject_term;
INSERT INTO findingaids_devel.subject_term
	(id, subject_id, term_id)
SELECT
	source_st.id + CEILING((@max_subject_term + 1)/1000.0) * 1000,
    sm.target_id, tm.target_id
FROM archivesspace_prod.subject_term source_st
	LEFT JOIN subject_mapping sm 
		ON sm.source_id = source_st.subject_id
    LEFT JOIN term_mapping tm 
		ON tm.source_id = source_st.term_id;
SET SQL_BIG_SELECTS = 0;

## telephone
SELECT @max_telephone:=COALESCE(MAX(id),1) FROM findingaids_devel.telephone;
INSERT INTO findingaids_devel.telephone
	(id, agent_contact_id, `number`, ext, created_by, last_modified_by,
    create_time, system_mtime, user_mtime, number_type_id)
SELECT
	source_t.id + CEILING((@max_telephone + 1)/1000.0) * 1000, 
    source_t.agent_contact_id + CEILING((@max_agent_contact + 1)/1000.0) * 1000, 
    source_t.`number`, source_t.ext, source_t.created_by, source_t.last_modified_by,
    source_t.create_time, source_t.system_mtime, source_t.user_mtime, em.target_id
FROM archivesspace_prod.telephone source_t
	LEFT JOIN enumeration_mapping em
		ON em.source_id = source_t.number_type_id;

## top_container
SELECT @max_top_container:=COALESCE(MAX(id),1) FROM findingaids_devel.top_container;
INSERT INTO findingaids_devel.top_container
    (id, repo_id, lock_version, json_schema_version, barcode,
    ils_holding_id, ils_item_id, exported_to_ils, indicator, created_by, last_modified_by, create_time,
    system_mtime, user_mtime, type_id, created_for_collection)
SELECT
    source.id + CEILING((@max_top_container + 1)/1000.0) * 1000,
    @target_repo_id,
    source.lock_version, source.json_schema_version, source.barcode,
    source.ils_holding_id, source.ils_item_id, source.exported_to_ils,
    source.indicator, source.created_by, source.last_modified_by, source.create_time,
    source.system_mtime, source.user_mtime,
    target_e.id,
    source.created_for_collection
FROM archivesspace_prod.top_container AS source
LEFT JOIN archivesspace_prod.enumeration_value AS source_e ON (source.type_id = source_e.id)
LEFT JOIN findingaids_devel.enumeration_value AS target_e ON (target_e.value = REPLACE(LOWER(source_e.value), ' ','-'));

####################################################
###               Wave 4 Tables                  ###
####################################################

## archival_object
SELECT @max_archival_object:=COALESCE(MAX(id),1) FROM findingaids_devel.archival_object;
SET FOREIGN_KEY_CHECKS=0; # doing this for the parent_id field
INSERT INTO findingaids_devel.archival_object
	(id, lock_version, json_schema_version, repo_id,
    root_record_id, parent_id, parent_name, position,
    publish, ref_id, component_id, title, display_string,
    level_id, other_level, system_generated, restrictions_apply,
    repository_processing_note, created_by, last_modified_by, create_time, system_mtime,
    user_mtime, suppressed, slug, is_slug_auto, external_ark_url)
SELECT
	source_a.id + CEILING((@max_archival_object + 1)/1000.0) * 1000, 
    source_a.lock_version, source_a.json_schema_version, @target_repo_id,
    source_a.root_record_id + CEILING((@max_resource + 1)/1000.0) * 1000, 
    source_a.parent_id + CEILING((@max_archival_object + 1)/1000.0) * 1000,
    REPLACE(
		REPLACE(
			REPLACE(source_a.parent_name,CONCAT('root@/repositories/', @source_repo_id),CONCAT('root@/repositories/', @target_repo_id)),
			COALESCE(CONCAT(source_a.parent_id, "@archival_object"),""),
			CONCAT(source_a.parent_id + CEILING((@max_archival_object + 1)/1000.0) * 1000, "@archival_object")),
		COALESCE(CONCAT("resources/", source_a.root_record_id),""),
        CONCAT("resources/", source_a.root_record_id + CEILING((@max_resource + 1)/1000.0) * 1000)),
    source_a.position, source_a.publish, source_a.ref_id, source_a.component_id, source_a.title, source_a.display_string,
    em.target_id, source_a.other_level, source_a.system_generated, source_a.restrictions_apply,
    source_a.repository_processing_note, source_a.created_by, source_a.last_modified_by, source_a.create_time, source_a.system_mtime,
    source_a.user_mtime, source_a.suppressed, source_a.slug, source_a.is_slug_auto, 
    source_a.external_ark_url
FROM archivesspace_prod.archival_object source_a
	LEFT JOIN enumeration_mapping em
		ON em.source_id = source_a.level_id;
SET FOREIGN_KEY_CHECKS=1;


## file_version
SELECT @max_file_version:=COALESCE(MAX(id),1) FROM findingaids_devel.file_version;
SET SQL_BIG_SELECTS = 1;
INSERT INTO findingaids_devel.file_version
	(id, lock_version, json_schema_version, digital_object_id,
    digital_object_component_id, use_statement_id, checksum_method_id,
    file_uri, publish, xlink_actuate_attribute_id,
    xlink_show_attribute_id, file_format_name_id, file_format_version,
    file_size_bytes, `checksum`, checksum_method, created_by,
    last_modified_by, create_time, system_mtime, user_mtime,
    is_representative, caption)
SELECT
	source_f.id + CEILING((@max_file_version + 1)/1000.0) * 1000, source_f.lock_version, 
    source_f.json_schema_version, source_f.digital_object_id + CEILING((@max_digital_object + 1)/1000.0) * 1000,
    NULL, em_u.target_id, em_c.target_id,
    source_f.file_uri, publish, em_xa.target_id,
    em_xs.target_id, em_f.target_id, source_f.file_format_version,
    source_f.file_size_bytes, source_f.`checksum`, source_f.checksum_method, source_f.created_by,
    source_f.last_modified_by, source_f.create_time, source_f.system_mtime, source_f.user_mtime,
    source_f.is_representative, source_f.caption
FROM archivesspace_prod.file_version source_f
	LEFT JOIN enumeration_mapping em_u
		ON em_u.source_id = source_f.use_statement_id
	LEFT JOIN enumeration_mapping em_c
		ON em_c.source_id = source_f.checksum_method_id
	LEFT JOIN enumeration_mapping em_xa
		ON em_xa.source_id = source_f.xlink_actuate_attribute_id
	LEFT JOIN enumeration_mapping em_xs
		ON em_xs.source_id = source_f.xlink_show_attribute_id
	LEFT JOIN enumeration_mapping em_f
		ON em_f.source_id = source_f.file_format_name_id;
SET SQL_BIG_SELECTS = 0;

## group_permission
SELECT @max_group_permission:=COALESCE(MAX(id),1) FROM findingaids_devel.group_permission;
INSERT INTO findingaids_devel.group_permission
	(id, permission_id, group_id)
SELECT
	source_gp.id + CEILING((@max_group_permission + 1)/1000.0) * 1000,
    target_p.id, gm.target_id
FROM archivesspace_prod.group_permission source_gp
	LEFT JOIN group_mapping gm
		ON gm.source_id = source_gp.group_id
	LEFT JOIN archivesspace_prod.group source_g
		ON source_g.id = gm.source_id
	LEFT JOIN archivesspace_prod.permission source_p
		ON source_p.id = source_gp.permission_id
	LEFT JOIN findingaids_devel.permission target_p
		ON target_p.permission_code = source_p.permission_code
WHERE source_g.repo_id = @source_repo_id
ON DUPLICATE KEY UPDATE group_id = gm.target_id;

## group_user
SELECT @max_group_user:=COALESCE(MAX(id),1) FROM findingaids_devel.group_user;
INSERT INTO findingaids_devel.group_user
	(id, user_id, group_id)
SELECT
	source_g.id + CEILING((@max_group_user + 1)/1000.0) * 1000,
    target_u.id,
    gm.target_id
FROM archivesspace_prod.group_user source_g
	LEFT JOIN group_mapping gm
		ON gm.source_id = source_g.group_id
	LEFT JOIN archivesspace_prod.group source_gp
		ON source_gp.id = gm.source_id
	LEFT JOIN archivesspace_prod.`user` source_u
		ON source_u.id = source_g.user_id
	LEFT JOIN findingaids_devel.`user` target_u
		ON target_u.username = source_u.username
WHERE source_gp.repo_id = @source_repo_id;


## import_job_input_file

## job_input_file


## revision_statement
SELECT @max_revision_statement := COALESCE(MAX(id),1) FROM findingaids_devel.revision_statement;
INSERT INTO findingaids_devel.revision_statement
	(id, resource_id, `date`, `description`, created_by, last_modified_by, create_time, system_mtime, user_mtime, publish)
SELECT
	source_r.id + CEILING((@max_revision_statement + 1)/1000.0) * 1000, 
    source_r.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000, 
    source_r.`date`, source_r.`description`, source_r.created_by, source_r.last_modified_by, 
    source_r.create_time, source_r.system_mtime, source_r.user_mtime, source_r.publish
FROM archivesspace_prod.revision_statement source_r;

## top_container_housed_at_rlshp
SELECT @max_top_container_housed_at_rlshp := COALESCE(MAX(id),1) FROM findingaids_devel.top_container_housed_at_rlshp;
INSERT INTO findingaids_devel.top_container_housed_at_rlshp 
	(id, top_container_id, location_id, aspace_relationship_position, suppressed,
     jsonmodel_type, `status`, start_date, end_date, note, created_by, last_modified_by,
     system_mtime, user_mtime)
SELECT 
    source_t.id + CEILING((@max_top_container_housed_at_rlshp + 1)/1000.0) * 1000,
    source_t.top_container_id + CEILING((@max_top_container + 1)/1000.0) * 1000,
    source_t.location_id + CEILING((@max_location + 1)/1000.0) * 1000, 
    source_t.aspace_relationship_position,
    source_t.suppressed,
    source_t.jsonmodel_type,
    source_t.`status`,
    source_t.start_date,
    source_t.end_date,
    source_t.note,
    source_t.created_by,
    source_t.last_modified_by,
    source_t.system_mtime,
    source_t.user_mtime
FROM archivesspace_prod.top_container_housed_at_rlshp AS source_t;

## top_container_profile_rlshp
SELECT @max_top_container_profile_rlshp := COALESCE(MAX(id),1) FROM findingaids_devel.top_container_profile_rlshp;
INSERT INTO findingaids_devel.top_container_profile_rlshp
	(id, top_container_id, container_profile_id, aspace_relationship_position,
    suppressed, created_by, last_modified_by, system_mtime, user_mtime)
SELECT
	source_t.id + CEILING((@max_top_container_profile_rlshp + 1)/1000.0) * 1000, 
    source_t.top_container_id + CEILING((@max_top_container + 1)/1000.0) * 1000, 
    source_t.container_profile_id + CEILING((@max_container_profile + 1)/1000.0) * 1000, 
    source_t.aspace_relationship_position, source_t.suppressed, source_t.created_by, 
    source_t.last_modified_by, source_t.system_mtime, source_t.user_mtime
FROM archivesspace_prod.top_container_profile_rlshp source_t;

####################################################
###               Wave 5 Tables                  ###
####################################################

## collection_management
SELECT @max_collection_management := COALESCE(MAX(id),1) FROM findingaids_devel.collection_management;
SET SQL_BIG_SELECTS = 1;
INSERT INTO findingaids_devel.collection_management
	(id, lock_version, json_schema_version, accession_id, resource_id,
    digital_object_id, processing_hours_per_foot_estimate, processing_total_extent,
    processing_total_extent_type_id, processing_hours_total, processing_plan, 
    processing_priority_id, processing_status_id, processing_funding_source, 
    processors, rights_determined, created_by, last_modified_by, create_time, system_mtime, user_mtime)
SELECT
	source_c.id + CEILING((@max_collection_management + 1)/1000.0) * 1000, 
    source_c.lock_version, source_c.json_schema_version, 
    source_c.accession_id + CEILING((@max_accession + 1)/1000.0) * 1000, 
    source_c.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000,
    source_c.digital_object_id, source_c.processing_hours_per_foot_estimate, source_c.processing_total_extent,
    em_extent_type.target_id, source_c.processing_hours_total, source_c.processing_plan, 
    em_priority.target_id, em_status.target_id, source_c.processing_funding_source, 
    source_c.processors, source_c.rights_determined, source_c.created_by, source_c.last_modified_by, source_c.create_time, 
    source_c.system_mtime, source_c.user_mtime
FROM archivesspace_prod.collection_management source_c
	LEFT JOIN enumeration_mapping em_extent_type
		ON em_extent_type.source_id = source_c.processing_total_extent_type_id
	LEFT JOIN enumeration_mapping em_priority
		ON em_priority.source_id = source_c.processing_priority_id
	LEFT JOIN enumeration_mapping em_status
		ON em_status.source_id = source_c.processing_status_id;
SET SQL_BIG_SELECTS = 0;

## deaccession
SELECT @max_deaccession := COALESCE(MAX(id),1) FROM findingaids_devel.deaccession;
INSERT INTO findingaids_devel.deaccession
	(id, lock_version, json_schema_version, accession_id,
    resource_id, scope_id, `description`, reason, disposition, notification,
    created_by, last_modified_by, create_time, system_mtime, user_mtime)
SELECT
	source_d.id + CEILING((@max_deaccession + 1)/1000.0) * 1000, 
    source_d.lock_version, source_d.json_schema_version, source_d.accession_id + CEILING((@max_accession + 1)/1000.0) * 1000,
    source_d.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000, em_scope.target_id, source_d.`description`, source_d.reason, 
    source_d.disposition, source_d.notification, source_d.created_by, source_d.last_modified_by, source_d.create_time, 
    source_d.system_mtime, source_d.user_mtime
FROM archivesspace_prod.deaccession source_d
	LEFT JOIN enumeration_mapping em_scope
		ON em_scope.source_id = source_d.scope_id;

## event_link_rlshp
SELECT @max_event_link_rlshp := COALESCE(MAX(id),1) FROM findingaids_devel.event_link_rlshp;
SET SQL_BIG_SELECTS = 1;
INSERT INTO findingaids_devel.event_link_rlshp
	(id, accession_id, resource_id, archival_object_id,
    digital_object_id, digital_object_component_id, agent_person_id,
    agent_family_id, agent_corporate_entity_id, agent_software_id, event_id,
    aspace_relationship_position, created_by, last_modified_by, system_mtime,
    user_mtime, role_id, suppressed, top_container_id)
SELECT
	source_e.id + CEILING((@max_event_link_rlshp + 1)/1000.0) * 1000, 
    source_e.accession_id + CEILING((@max_accession + 1)/1000.0) * 1000, 
    source_e.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000, 
    source_e.archival_object_id + CEILING((@max_archival_object + 1)/1000.0) * 1000,
    source_e.digital_object_id + CEILING((@max_digital_object + 1)/1000.0) * 1000, source_e.digital_object_component_id, 
    source_e.agent_person_id + CEILING((@max_agent_person + 1)/1000.0) * 1000,
    source_e.agent_family_id + CEILING((@max_agent_family + 1)/1000.0) * 1000, 
    acm.target_id, target_as.id, source_e.event_id + CEILING((@max_event + 1)/1000.0) * 1000,
    source_e.aspace_relationship_position, source_e.created_by, source_e.last_modified_by, source_e.system_mtime,
    source_e.user_mtime, em_role.target_id, source_e.suppressed, source_e.top_container_id + CEILING((@max_top_container + 1)/1000.0) * 1000
FROM archivesspace_prod.event_link_rlshp source_e
	LEFT JOIN enumeration_mapping em_role
		ON em_role.source_id = source_e.role_id
	# Map agent_software ID
	LEFT JOIN archivesspace_prod.agent_software source_as
		ON source_as.id = source_e.agent_software_id
	# Map agent_software to target ID
	LEFT JOIN findingaids_devel.agent_software target_as
		ON target_as.system_role = source_as.system_role
	# Map agent corporate entity
	LEFT JOIN agent_corporate_entity_mapping acm
		ON acm.source_id = source_e.agent_corporate_entity_id;
SET SQL_BIG_SELECTS = 0;

## instance
SELECT @max_instance:= COALESCE(MAX(id),1) FROM findingaids_devel.`instance`;
INSERT INTO findingaids_devel.`instance`
	(id, lock_version, json_schema_version, resource_id, archival_object_id,
    accession_id, instance_type_id, created_by, last_modified_by, create_time,
    system_mtime, user_mtime, is_representative)
SELECT
	source_i.id + CEILING((@max_instance + 1)/1000.0) * 1000, 
    source_i.lock_version, source_i.json_schema_version, 
    source_i.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000, 
    source_i.archival_object_id + CEILING((@max_archival_object + 1)/1000.0) * 1000,
    source_i.accession_id + CEILING((@max_accession + 1)/1000.0) * 1000, 
    em_instance_type.target_id, source_i.created_by, source_i.last_modified_by, source_i.create_time,
    source_i.system_mtime, source_i.user_mtime, source_i.is_representative
FROM archivesspace_prod.`instance` source_i
	LEFT JOIN enumeration_mapping em_instance_type
		ON em_instance_type.source_id = source_i.instance_type_id;


## lang_material
SELECT @max_lang_material:= COALESCE(MAX(id),1) FROM findingaids_devel.lang_material;
INSERT INTO findingaids_devel.lang_material
	(id, lock_version, json_schema_version, archival_object_id, resource_id,
    digital_object_id, digital_object_component_id, created_by,
    last_modified_by, create_time, system_mtime, user_mtime)
SELECT
	source_l.id + CEILING((@max_lang_material + 1)/1000.0) * 1000, 
    source_l.lock_version, source_l.json_schema_version, 
    source_l.archival_object_id + CEILING((@max_archival_object + 1)/1000.0) * 1000, 
    source_l.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000,
    source_l.digital_object_id + CEILING((@max_digital_object + 1)/1000.0) * 1000, NULL, source_l.created_by,
    source_l.last_modified_by, source_l.create_time, source_l.system_mtime, source_l.user_mtime
FROM archivesspace_prod.lang_material source_l;


## rights_restriction
SELECT @max_rights_restriction:= COALESCE(MAX(id),1) FROM findingaids_devel.rights_restriction;
INSERT INTO findingaids_devel.rights_restriction
	(id, resource_id, archival_object_id, restriction_note_type, `begin`, `end`)
SELECT
	source_r.id + CEILING((@max_rights_restriction + 1)/1000.0) * 1000, 
    source_r.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000, 
    source_r.archival_object_id + CEILING((@max_archival_object + 1)/1000.0) * 1000, 
    source_r.restriction_note_type, source_r.`begin`, source_r.`end`
FROM archivesspace_prod.rights_restriction source_r;


## rights_statement
SELECT @max_rights_statement:= COALESCE(MAX(id),1) FROM findingaids_devel.rights_statement;
SET SQL_BIG_SELECTS = 1;
INSERT INTO findingaids_devel.rights_statement
	(id, lock_version, json_schema_version, accession_id,
    archival_object_id, resource_id, digital_object_id, digital_object_component_id,
    repo_id, identifier, rights_type_id, statute_citation, jurisdiction_id,
    created_by, last_modified_by, create_time, system_mtime, user_mtime, status_id,
    start_date, end_date, determination_date, license_terms, other_rights_basis_id)
SELECT
	source_r.id + CEILING((@max_rights_statement + 1)/1000.0) * 1000,  
    source_r.lock_version, source_r.json_schema_version, 
    source_r.accession_id + CEILING((@max_accession + 1)/1000.0) * 1000,
    source_r.archival_object_id + CEILING((@max_archival_object + 1)/1000.0) * 1000, 
    source_r.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000,
    source_r.digital_object_id + CEILING((@max_digital_object + 1)/1000.0) * 1000, NULL,
    @target_repo_id, source_r.identifier, em_rights_type.target_id, source_r.statute_citation, em_jurisdiction.target_id,
    source_r.created_by, source_r.last_modified_by, source_r.create_time, source_r.system_mtime, source_r.user_mtime, 
    em_status.target_id,
    source_r.start_date, source_r.end_date, source_r.determination_date, source_r.license_terms, em_other_rights.target_id
FROM archivesspace_prod.rights_statement source_r
	LEFT JOIN enumeration_mapping em_rights_type
		ON em_rights_type.source_id = source_r.rights_type_id
	LEFT JOIN enumeration_mapping em_jurisdiction
		ON em_jurisdiction.source_id = source_r.jurisdiction_id
	LEFT JOIN enumeration_mapping em_other_rights
		ON em_other_rights.source_id = source_r.other_rights_basis_id
	LEFT JOIN enumeration_mapping em_status
		ON em_status.source_id = source_r.status_id;
SET SQL_BIG_SELECTS = 0;


## spawned_rlshp
SELECT @max_spawned_rlshp:=COALESCE(MAX(id),1) FROM findingaids_devel.spawned_rlshp;
INSERT INTO findingaids_devel.spawned_rlshp
	(id, accession_id, resource_id, aspace_relationship_position,
    created_by, last_modified_by, system_mtime, user_mtime, suppressed)
SELECT
	source_s.id + CEILING((@max_spawned_rlshp + 1)/1000.0) * 1000,
    source_s.accession_id + CEILING((@max_accession + 1)/1000.0) * 1000,
    source_s.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000,
    source_s.aspace_relationship_position, source_s.created_by, source_s.last_modified_by, 
    source_s.system_mtime, source_s.user_mtime, source_s.suppressed
FROM archivesspace_prod.spawned_rlshp source_s;

## subject_rlshp
SELECT @max_subject_rlshp:=COALESCE(MAX(id),1) FROM findingaids_devel.subject_rlshp;
INSERT INTO findingaids_devel.subject_rlshp
	(id, accession_id, archival_object_id, resource_id, digital_object_id,
    digital_object_component_id, subject_id, aspace_relationship_position,
    created_by, last_modified_by, system_mtime, user_mtime, suppressed)
SELECT
	source_s.id + CEILING((@max_subject_rlshp + 1)/1000.0) * 1000,
    source_s.accession_id + CEILING((@max_accession + 1)/1000.0) * 1000,
    source_s.archival_object_id + CEILING((@max_archival_object + 1)/1000.0) * 1000,
    source_s.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000,
    source_s.digital_object_id + CEILING((@max_digital_object + 1)/1000.0) * 1000,
    NULL, sub_mapping.target_id, source_s.aspace_relationship_position,
    source_s.created_by, source_s.last_modified_by, source_s.system_mtime, source_s.user_mtime, source_s.suppressed
FROM archivesspace_prod.subject_rlshp source_s
	LEFT JOIN subject_mapping sub_mapping
		ON sub_mapping.source_id = source_s.subject_id;


## user_defined
SELECT @max_user_defined:=COALESCE(MAX(id),1) FROM findingaids_devel.user_defined;
SET SQL_BIG_SELECTS = 1;
INSERT INTO findingaids_devel.user_defined
	(id, lock_version, json_schema_version, accession_id, resource_id,
    digital_object_id, boolean_1, boolean_2, boolean_3,
    integer_1, integer_2, integer_3, real_1, real_2, real_3,
    string_1, string_2, string_3, string_4, text_1, text_2,
    text_3, text_4, text_5, date_1, date_2, date_3, created_by,
    last_modified_by, create_time, system_mtime, user_mtime,
    enum_1_id, enum_2_id, enum_3_id, enum_4_id)
SELECT
	source_u.id + CEILING((@max_user_defined + 1)/1000.0) * 1000,
    source_u.lock_version, source_u.json_schema_version, 
    source_u.accession_id + CEILING((@max_accession + 1)/1000.0) * 1000, 
    source_u.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000,
    source_u.digital_object_id + CEILING((@max_digital_object + 1)/1000.0) * 1000,
    source_u.boolean_1, source_u.boolean_2, source_u.boolean_3,
    source_u.integer_1, source_u.integer_2, source_u.integer_3, source_u.real_1, source_u.real_2, source_u.real_3,
    source_u.string_1, source_u.string_2, source_u.string_3, source_u.string_4, source_u.text_1, source_u.text_2,
    source_u.text_3, source_u.text_4, source_u.text_5, source_u.date_1, source_u.date_2, source_u.date_3, source_u.created_by,
    source_u.last_modified_by, source_u.create_time, source_u.system_mtime, source_u.user_mtime,
    e1.target_id, e2.target_id, e3.target_id, e4.target_id
FROM archivesspace_prod.user_defined source_u
	LEFT JOIN enumeration_mapping e1
		ON e1.source_id = source_u.enum_1_id
	LEFT JOIN enumeration_mapping e2
		ON e2.source_id = source_u.enum_2_id
	LEFT JOIN enumeration_mapping e3
		ON e3.source_id = source_u.enum_3_id
	LEFT JOIN enumeration_mapping e4
		ON e4.source_id = source_u.enum_4_id;
SET SQL_BIG_SELECTS = 0;


####################################################
###               Wave 6 Tables                  ###
####################################################


## date
SELECT @max_date:=COALESCE(MAX(id),1) FROM findingaids_devel.`date`;
SET SQL_BIG_SELECTS = 1;
INSERT INTO findingaids_devel.`date`
    (id,lock_version,json_schema_version,accession_id,deaccession_id,
    archival_object_id,resource_id,event_id,digital_object_id,
    digital_object_component_id,related_agents_rlshp_id,agent_person_id,
    agent_family_id,agent_corporate_entity_id,agent_software_id,
    name_person_id,name_family_id,name_corporate_entity_id,name_software_id,
    date_type_id,label_id,certainty_id,expression,`begin`,`end`,era_id,
    calendar_id,created_by,last_modified_by,create_time,system_mtime,user_mtime)
SELECT
    source_d.id + CEILING((@max_date + 1)/1000.0) * 1000,
    source_d.lock_version,
    source_d.json_schema_version,
    source_d.accession_id + CEILING((@max_accession + 1)/1000.0) * 1000,
    source_d.deaccession_id + CEILING((@max_deaccession + 1)/1000.0) * 1000,
    source_d.archival_object_id + CEILING((@max_archival_object + 1)/1000.0) * 1000,
    source_d.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000,
    source_d.event_id + CEILING((@max_event + 1)/1000.0) * 1000, 
    source_d.digital_object_id + CEILING((@max_digital_object + 1)/1000.0) * 1000,
    source_d.digital_object_component_id,
    source_d.related_agents_rlshp_id,
    source_d.agent_person_id + CEILING((@max_agent_person + 1)/1000.0) * 1000,
    source_d.agent_family_id + CEILING((@max_agent_family + 1)/1000.0) * 1000,
    acm.target_id,
    source_d.agent_software_id,
    source_d.name_person_id + CEILING((@max_name_person + 1)/1000.0) * 1000,
    source_d.name_family_id + CEILING((@max_name_family + 1)/1000.0) * 1000,
    nc_map.target_id,
    target_ns.id,
    em_date_type.target_id,
    em_label.target_id,
    em_certainty.target_id,
    source_d.expression,
    source_d.`begin`,
    source_d.`end`,
    em_era.target_id,
    em_calendar.target_id,
    source_d.created_by,
    source_d.last_modified_by,
    source_d.create_time,
    source_d.system_mtime,
    source_d.user_mtime
FROM archivesspace_prod.`date` source_d
-- Map name corporate entity
LEFT JOIN name_corporate_entity_mapping nc_map
	ON nc_map.source_id = source_d.name_corporate_entity_id
-- Map date_type_id
LEFT JOIN enumeration_mapping em_date_type
    ON em_date_type.source_id = source_d.date_type_id
-- Map label_id
LEFT JOIN enumeration_mapping em_label
    ON em_label.source_id = source_d.label_id
-- Map certainty_id
LEFT JOIN enumeration_mapping em_certainty
    ON em_certainty.source_id = source_d.certainty_id
-- Map era_id
LEFT JOIN enumeration_mapping em_era
    ON em_era.source_id = source_d.era_id
-- Map calendar_id
LEFT JOIN enumeration_mapping em_calendar
    ON em_calendar.source_id = source_d.calendar_id
-- Map name_software_id
LEFT JOIN archivesspace_prod.name_software source_ns
    ON source_ns.id = source_d.name_software_id
LEFT JOIN findingaids_devel.name_software target_ns
    ON target_ns.software_name = source_ns.software_name
LEFT JOIN agent_corporate_entity_mapping acm
	ON acm.source_id = source_d.agent_corporate_entity_id
;
SET SQL_BIG_SELECTS = 0;

## extent
SELECT @max_extent:=COALESCE(MAX(id),1) FROM findingaids_devel.extent;
SET SQL_BIG_SELECTS = 1;
INSERT INTO findingaids_devel.extent
    (id,lock_version,json_schema_version,accession_id,deaccession_id,
    archival_object_id,resource_id,digital_object_id,digital_object_component_id,
    portion_id,`number`,extent_type_id,container_summary,physical_details,
    dimensions,created_by,last_modified_by,create_time,system_mtime,user_mtime)
SELECT
    source_e.id + CEILING((@max_extent + 1)/1000.0) * 1000, 
    source_e.lock_version,
    source_e.json_schema_version,
    source_e.accession_id + CEILING((@max_accession + 1)/1000.0) * 1000,
    source_e.deaccession_id + CEILING((@max_deaccession + 1)/1000.0) * 1000,
    source_e.archival_object_id + CEILING((@max_archival_object + 1)/1000.0) * 1000,
    source_e.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000,
    source_e.digital_object_id + CEILING((@max_digital_object + 1)/1000.0) * 1000,
    source_e.digital_object_component_id,
    em_portion.target_id,
    source_e.`number`,
    em_extent_type.target_id,
    source_e.container_summary,
    source_e.physical_details,
    source_e.dimensions,
    source_e.created_by,
    source_e.last_modified_by,
    source_e.create_time,
    source_e.system_mtime,
    source_e.user_mtime
FROM archivesspace_prod.extent source_e
-- Map portion_id
LEFT JOIN enumeration_mapping em_portion
	ON em_portion.source_id = source_e.portion_id
-- Map date_type_id
LEFT JOIN enumeration_mapping em_extent_type
    ON em_extent_type.source_id = source_e.extent_type_id
;
SET SQL_BIG_SELECTS = 0;

## external_document
SELECT @max_external_document:=COALESCE(MAX(id),1) FROM findingaids_devel.external_document;
INSERT INTO findingaids_devel.external_document
	(id, lock_version, json_schema_version, title, location,
    publish, created_by, last_modified_by, create_time, system_mtime,
    user_mtime, location_sha1, accession_id, archival_object_id,
    resource_id, subject_id, agent_person_id, agent_family_id,
    agent_corporate_entity_id, agent_software_id, rights_statement_id,
    digital_object_id, digital_object_component_id, event_id, identifier_type_id)
SELECT
	source_e.id + CEILING((@max_external_document + 1)/1000.0) * 1000, 
    source_e.lock_version, source_e.json_schema_version, source_e.title, source_e.location,
    source_e.publish, source_e.created_by, source_e.last_modified_by, source_e.create_time, source_e.system_mtime,
    source_e.user_mtime, source_e.location_sha1, source_e.accession_id + CEILING((@max_accession + 1)/1000.0) * 1000,
    source_e.archival_object_id + CEILING((@max_archival_object + 1)/1000.0) * 1000,
    source_e.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000, sm_sub.target_id, 
    source_e.agent_person_id + CEILING((@max_agent_person + 1)/1000.0) * 1000, source_e.agent_family_id + CEILING((@max_agent_family + 1)/1000.0) * 1000,
    source_e.agent_corporate_entity_id + CEILING((@max_corporate_entit + 1)/1000.0) * 1000, 
    target_as.id, source_e.rights_statement_id + CEILING((@max_rights_statement + 1)/1000.0) * 1000,
    source_e.digital_object_id + CEILING((@max_resource + 1)/1000.0) * 1000, NULL, 
    source_e.event_id + CEILING((@max_resource + 1)/1000.0) * 1000, em_id.target_id
FROM archivesspace_prod.external_document source_e
	LEFT JOIN enumeration_mapping em_id
		ON em_id.source_id = source_e.identifier_type_id
	LEFT JOIN subject_mapping sm_sub
		ON sm_sub.source_id = source_e.subject_id
	LEFT JOIN archivesspace_prod.agent_software source_as
        ON source_as.id = source_e.agent_software_id
	LEFT JOIN findingaids_devel.agent_software target_as
        ON target_as.system_role = source_as.system_role;


## instance_do_link_rlshp
SELECT @max_instance_do_link_rlshp:=COALESCE(MAX(id),1) FROM findingaids_devel.instance_do_link_rlshp;
INSERT INTO findingaids_devel.instance_do_link_rlshp
	(id, digital_object_id, instance_id, aspace_relationship_position,
    created_by, last_modified_by, system_mtime, user_mtime, suppressed)
SELECT
	source_i.id + CEILING((@max_instance_do_link_rlshp + 1)/1000.0) * 1000, 
    source_i.digital_object_id + CEILING((@max_digital_object + 1)/1000.0) * 1000, 
    source_i.instance_id + CEILING((@max_instance + 1)/1000.0) * 1000, source_i.aspace_relationship_position,
    source_i.created_by, source_i.last_modified_by, source_i.system_mtime, source_i.user_mtime, source_i.suppressed
FROM archivesspace_prod.instance_do_link_rlshp source_i;

## language_and_script
SELECT @max_language_and_script:=COALESCE(MAX(id),1) FROM findingaids_devel.language_and_script;
SET SQL_BIG_SELECTS = 1;
INSERT INTO findingaids_devel.language_and_script
	(id, lock_version, json_schema_version, lang_material_id, language_id,
    script_id, created_by, last_modified_by, create_time, system_mtime, user_mtime)
SELECT
	source_l.id + CEILING((@max_language_and_script + 1)/1000.0) * 1000, 
    source_l.lock_version, source_l.json_schema_version, 
    source_l.lang_material_id + CEILING((@max_lang_material + 1)/1000.0) * 1000, 
    em_lang.target_id, em_script.target_id, source_l.created_by, source_l.last_modified_by, 
    source_l.create_time, source_l.system_mtime, source_l.user_mtime
FROM archivesspace_prod.language_and_script source_l
	LEFT JOIN enumeration_mapping em_lang
		ON em_lang.source_id = source_l.language_id
	LEFT JOIN enumeration_mapping em_script
		ON em_script.source_id = source_l.script_id;
SET SQL_BIG_SELECTS = 0;

## linked_agents_rlshp
SELECT @max_linked_agents_rlshp:=COALESCE(MAX(id),1) FROM findingaids_devel.linked_agents_rlshp;
SET SQL_BIG_SELECTS = 1;
INSERT INTO findingaids_devel.linked_agents_rlshp
	(id, agent_person_id, agent_software_id, agent_family_id,
    agent_corporate_entity_id, accession_id, archival_object_id,
    digital_object_id, digital_object_component_id, event_id,
    resource_id, aspace_relationship_position, created_by,
    last_modified_by, create_time, system_mtime, user_mtime,
    role_id, relator_id, title, suppressed, rights_statement_id)
SELECT
	source_l.id + CEILING((@max_linked_agents_rlshp + 1)/1000.0) * 1000, 
    source_l.agent_person_id + CEILING((@max_agent_person + 1)/1000.0) * 1000, target_as.id, 
    source_l.agent_family_id + CEILING((@max_agent_family + 1)/1000.0) * 1000,
    acem.target_id,
    source_l.accession_id + CEILING((@max_accession + 1)/1000.0) * 1000,
    source_l.archival_object_id + CEILING((@max_archival_object + 1)/1000.0) * 1000,
    source_l.digital_object_id + CEILING((@max_digital_object + 1)/1000.0) * 1000, NULL, 
    source_l.event_id + CEILING((@max_event + 1)/1000.0) * 1000,
    source_l.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000, source_l.aspace_relationship_position, 
    source_l.created_by, source_l.last_modified_by, source_l.create_time, source_l.system_mtime, source_l.user_mtime,
    em_role.target_id, em_realtor.target_id, source_l.title, source_l.suppressed, 
    source_l.rights_statement_id + CEILING((@max_rights_statement + 1)/1000.0) * 1000
FROM archivesspace_prod.linked_agents_rlshp source_l
	LEFT JOIN agent_corporate_entity_mapping acem
		ON acem.source_id = agent_corporate_entity_id
	LEFT JOIN archivesspace_prod.agent_software source_as
        ON source_as.id = source_l.agent_software_id
	LEFT JOIN findingaids_devel.agent_software target_as
        ON target_as.system_role = source_as.system_role
	LEFT JOIN enumeration_mapping em_role
		ON em_role.source_id = source_l.role_id
	LEFT JOIN enumeration_mapping em_realtor
		ON em_realtor.source_id = source_l.relator_id;
SET SQL_BIG_SELECTS = 0;


## note
SELECT @max_note:=COALESCE(MAX(id),1) FROM findingaids_devel.note;
INSERT INTO findingaids_devel.note
	(id, lock_version, resource_id, archival_object_id, digital_object_id,
    digital_object_component_id, agent_person_id, agent_corporate_entity_id,
    agent_family_id, agent_software_id, publish, notes_json_schema_version,
    notes, created_by, last_modified_by, create_time, system_mtime, user_mtime,
    rights_statement_act_id, rights_statement_id, lang_material_id)
SELECT
	source_n.id + CEILING((@max_note + 1)/1000.0) * 1000, source_n.lock_version, 
    source_n.resource_id + CEILING((@max_resource + 1)/1000.0) * 1000, 
    source_n.archival_object_id + CEILING((@max_archival_object + 1)/1000.0) * 1000, 
    source_n.digital_object_id + CEILING((@max_digital_object + 1)/1000.0) * 1000,
    NULL, source_n.agent_person_id + CEILING((@max_agent_person + 1)/1000.0) * 1000, 
    acm.target_id,
    source_n.agent_family_id + CEILING((@max_agent_family + 1)/1000.0) * 1000, target_as.id, source_n.publish, 
    source_n.notes_json_schema_version, source_n.notes, source_n.created_by, source_n.last_modified_by, 
    source_n.create_time, source_n.system_mtime, source_n.user_mtime,
    NULL, source_n.rights_statement_id + CEILING((@max_rights_statement + 1)/1000.0) * 1000, 
    source_n.lang_material_id + CEILING((@max_lang_material + 1)/1000.0) * 1000
FROM archivesspace_prod.note source_n
	LEFT JOIN archivesspace_prod.agent_software source_as
        ON source_as.id = source_n.agent_software_id
	LEFT JOIN findingaids_devel.agent_software target_as
        ON target_as.system_role = source_as.system_role
	LEFT JOIN agent_corporate_entity_mapping acm
		ON acm.source_id = source_n.agent_corporate_entity_id;


## rights_restriction_type
SELECT @max_rights_restriction_type:=COALESCE(MAX(id),1) FROM findingaids_devel.rights_restriction_type;
INSERT INTO findingaids_devel.rights_restriction_type
	(id, rights_restriction_id, restriction_type_id)
SELECT
	source_r.id + CEILING((@max_rights_restriction_type + 1)/1000.0) * 1000, 
    source_r.rights_restriction_id + CEILING((@max_rights_restriction + 1)/1000.0) * 1000, 
    em_restriction_type.target_id
FROM archivesspace_prod.rights_restriction_type source_r
	LEFT JOIN enumeration_mapping em_restriction_type
		ON em_restriction_type.source_id = source_r.restriction_type_id;


## sub_container
SELECT @max_sub_container:=COALESCE(MAX(id),1) FROM findingaids_devel.sub_container;
SET SQL_BIG_SELECTS = 1;
INSERT INTO findingaids_devel.sub_container
	(id, lock_version, json_schema_version, instance_id, type_2_id,
    indicator_2, type_3_id, indicator_3, created_by,
    last_modified_by, create_time, system_mtime, user_mtime)
SELECT
	source_s.id + CEILING((@max_sub_container + 1)/1000.0) * 1000, 
    lock_version, source_s.json_schema_version, 
    source_s.instance_id + CEILING((@max_instance + 1)/1000.0) * 1000, em_type_2.target_id,
    source_s.indicator_2, em_type_3.target_id, source_s.indicator_3, source_s.created_by,
    source_s.last_modified_by, source_s.create_time, source_s.system_mtime, source_s.user_mtime
FROM archivesspace_prod.sub_container source_s
	LEFT JOIN enumeration_mapping em_type_2
		ON em_type_2.source_id = source_s.type_2_id
	LEFT JOIN enumeration_mapping em_type_3
		ON em_type_3.source_id = source_s.type_3_id;
SET SQL_BIG_SELECTS = 0;

####################################################
###               Wave 7 Tables                  ###
####################################################

## linked_agent_term
SELECT @max_linked_agent_term:=COALESCE(MAX(id),1) FROM findingaids_devel.linked_agent_term;
INSERT INTO findingaids_devel.linked_agent_term
	(id, linked_agents_rlshp_id, term_id)
SELECT
	source_l.id + CEILING((@max_linked_agent_term + 1)/1000.0) * 1000, 
    source_l.linked_agents_rlshp_id + CEILING((@max_linked_agents_rlshp + 1)/1000.0) * 1000, 
    tm_term.target_id
FROM archivesspace_prod.linked_agent_term source_l
	LEFT JOIN term_mapping tm_term
		ON tm_term.source_id = source_l.term_id;


## note_persistent_id
SELECT DISTINCT parent_type FROM archivesspace_prod.note_persistent_id;
#### !!! Before running this make sure the parent_types are all covered in case statement below
SELECT @max_note_persistent_id:=COALESCE(MAX(id),1) FROM findingaids_devel.note_persistent_id;
INSERT INTO findingaids_devel.note_persistent_id
	(id, note_id, persistent_id, parent_type, parent_id)
SELECT
	source_n.id + CEILING((@max_note_persistent_id + 1)/1000.0) * 1000, 
    source_n.note_id + CEILING((@max_note + 1)/1000.0) * 1000, 
    source_n.persistent_id, source_n.parent_type, 
    CASE 
		WHEN source_n.parent_type = "agent_person" 
			THEN source_n.parent_id + CEILING((@max_agent_person + 1)/1000.0) * 1000
		WHEN source_n.parent_type = "agent_family" 
			THEN source_n.parent_id + CEILING((@max_agent_family + 1)/1000.0) * 1000
		WHEN source_n.parent_type = "digital_object" 
			THEN source_n.parent_id + CEILING((@max_digital_object + 1)/1000.0) * 1000
		WHEN source_n.parent_type = "resource" 
			THEN source_n.parent_id + CEILING((@max_resource + 1)/1000.0) * 1000
		ELSE
			source_n.parent_id
	END
FROM archivesspace_prod.note_persistent_id source_n;


## subnote_metadata
SELECT @max_subnote_metadata:=COALESCE(MAX(id),1) FROM findingaids_devel.subnote_metadata;
INSERT INTO findingaids_devel.subnote_metadata
	(id, note_id, guid, publish)
SELECT
	source_s.id + CEILING((@max_subnote_metadata + 1)/1000.0) * 1000, 
    source_s.note_id + CEILING((@max_note + 1)/1000.0) * 1000, source_s.guid, source_s.publish
FROM archivesspace_prod.subnote_metadata source_s;


## top_container_link_rlshp
SELECT @max_top_container_link_rlshp:=COALESCE(MAX(id),1) FROM findingaids_devel.top_container_link_rlshp;
INSERT INTO findingaids_devel.top_container_link_rlshp
	(id, top_container_id, sub_container_id, aspace_relationship_position,
    suppressed, created_by, last_modified_by, system_mtime, user_mtime)
SELECT
	source_t.id + CEILING((@max_top_container_link_rlshp + 1)/1000.0) * 1000, 
    source_t.top_container_id + CEILING((@max_top_container + 1)/1000.0) * 1000, 
    source_t.sub_container_id + CEILING((@max_sub_container + 1)/1000.0) * 1000, 
    source_t.aspace_relationship_position, source_t.suppressed, source_t.created_by, 
    source_t.last_modified_by, source_t.system_mtime, source_t.user_mtime
FROM archivesspace_prod.top_container_link_rlshp source_t;

